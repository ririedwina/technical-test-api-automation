var chai = require('chai');
let {expect, assert} = require('chai');

let chaiHttp = require('chai-http');
chai.use(chaiHttp)

chai.use(require('chai-json-schema'));
const schema = require('../helper.json');

describe('API Automation Technical Test', function() {
    it("GET test for 20 result perpage", async function(){
        let res = await chai.request('https://api.punkapi.com/v2')
        .get('/beers?page=2&per_page=20')

        expect(res).to.have.status(200)
        expect(res.body.length).to.equal(20)
    })

    it("GET test for 5 result perpage", async function(){
        let res = await chai.request('https://api.punkapi.com/v2')
        .get('/beers?page=2&per_page=5')

        expect(res).to.have.status(200)
        expect(res.body.length).to.equal(5)
    })

    it("GET test for 1 result perpage", async function(){
        let res = await chai.request('https://api.punkapi.com/v2')
        .get('/beers?page=2&per_page=1')

        expect(res).to.have.status(200)
        expect(res.body.length).to.equal(1)
    })

    it("GET test for verify schema", async function(){
        let res = await chai.request('https://api.punkapi.com/v2')
        .get('/beers')

        expect(res.body).to.be.jsonSchema(schema.validSchema);
        expect(res).to.have.status(200)
    })

    it("GET test for verifying amount of data", async function(){
        let res = await chai.request('https://api.punkapi.com/v2')
        .get('/beers')

        expect(res).to.have.status(200)
        expect(res.body.length).to.equal(25)
    })

    it("GET test for printing all returned “name” of list", async function(){
        const name = []
        let res = await chai.request('https://api.punkapi.com/v2')
        .get('/beers')

        expect(res).to.have.status(200)
        console.log('Name:')
        for (let index = 0; index < res.body.length; index++) {
           console.log(res.body[index].name)
        }
    })  
})
